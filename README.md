# final_project
Final aos573 project

## description

This project attempts to identify areas and periods of heatwaves, and subsequently tries to find different ways to quantify a heatwave. 

# contents
The repo contains a NetCDF file of daily mean wet bulb temperature as well as a jupyter notebook that analyzes the netCDF file
Also contains a environment.yml file 



